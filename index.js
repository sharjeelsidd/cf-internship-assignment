/* Go to https://sharjeel-cfi.sharjeelsidd.workers.dev to view the Challenge
Author: Sharjeel Siddique
*/

addEventListener('fetch', event => {
	event.respondWith(handleRequest(event.request))
  })
  async function getStuff(url, type) {
	try {
	  const res = await fetch(url)
	  if (type === 'text') {
		return res.text()
	  }
	  return res.json()
	} catch (err) {
	  console.error('Caught error while fetching ->', err.message, err)
	  throw err
	}
  }
  
  /**
   * Respond with hello worker text
   * @param {Request} request
   */
  async function handleRequest(request) {
	
	const {
	  variants
	} = await getStuff('https://cfw-takehome.developers.workers.dev/api/variants')
  
	const NAME = 'sharjeel-cf'
	const variant1 = new Response(await getStuff(variants[0], 'text'), {
	  headers: {
		'content-type': 'text/html'
	  },
	  
	})
	const variant2 = new Response(await getStuff(variants[1], 'text'), {
	  headers: {
		'content-type': 'text/html'
	  },
	})
   // Setting up the cookie
	const cookie = request.headers.get('cookie')
  
	//HTML ReWriter Function
	function ReWriteFunction(headline, description, pageTitle, linkName, link, ){
	  return new HTMLRewriter() 
	  .on('h1', { element:  e => e.setInnerContent(headline,  { html: false }) })
	  .on('p#description', { element: e => e.setInnerContent(description,  { html: false }) })
	  .on('title', { element: e => e.setInnerContent(pageTitle,  { html: false }) })
	  .on('a', { element: e => e.setInnerContent(linkName,  { html: false }) })
	 .on('a', { element: e => e.setAttribute('href', link) })
	}
	const REWRITER = ReWriteFunction("Variant 1 Response", "This is a response modified for Variant 1", "Variant 1 - Visit my website","My website link",'https://sharjeelsidd.com')
  
	 const REWRITER_2 = ReWriteFunction("Variant 2 Response", "This is a response modified for Variant 2", "Variant 2 - My LinkedIn profile", "My Linkedin profile", 'https://www.linkedin.com/in/sharjeelsidd/');
	  
	if (cookie && cookie.includes(`${NAME}=1`)) {
	  console.log('first')
	  variant1.headers.set('Set-Cookie', `${NAME}=1; path=/`)
	  return REWRITER.transform(variant1)
	} else if (cookie && cookie.includes(`${NAME}=2`)) {
	  console.log('second')
	  variant2.headers.set('Set-Cookie', `${NAME}=2; path=/`)
	  return REWRITER_2.transform(variant2)
	} else if (!cookie) {
	  console.log('no-cookie')
	  // if no cookie then this is a new client, decide a group and set the cookie
	  let variant = Math.random() < 0.5 ? '1' : '2' // 50/50 split
	  let response = variant === '2'? REWRITER_2.transform(variant2) : REWRITER.transform(variant1)
	  response.headers.append('Set-Cookie', `${NAME}=${variant}; path=/`)
	  return response;
	}
	return new Response('', {
	  status: 200
	})
  }